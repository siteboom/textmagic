const ticketPlaceholder = {
    header: 'How to connect PayPal?',
    author: {
        name: 'Christopher Crawford',
        email: 'christopher@corp.com',
        logo: 'images/Avatar2.png',
        company: 'SalesForce',
        socials: [
            {
                icon: 'mdi-phone'
            },
            {
                icon: 'mdi-message-text'
            },
            {
                icon: 'mdi-currency-usd'
            },
            {
                icon: 'mdi-clipboard-check'
            },
            {
                icon: 'mdi-bell'
            }
        ]
    },
    published_at: 'Yesterday 12:22 am',
    tasks: [
        {
            title: 'Product demo call with Ruth',
            short_description: 'Please call and explain the customer about our product and answer.',
            priority: 'Task',
            step: 'Meeting',
            user: {
                logo: 'images/UserAvatar.png',
                name: 'Franklin Stewart'
            },
            status: 'Open',
            due_date: 'Due today'
        },
        {
            title: 'Key client yearly offer',
            short_description: 'Hello {First name}, Thank you for being our loyal customer. In this email we would...',
            priority: 'Deal',
            step: '$400K',
            user: {
                logo: 'images/UserAvatar.png',
                name: 'Jenny Wilson'
            },
            status: 'Negotiation',
            due_date: 'Due today'
        },
        {
            title: 'Product demo call with Ruth',
            short_description: 'Please call and explain the customer about our product and answer.',
            priority: 'Task',
            step: 'Meeting',
            user: {
                logo: 'images/UserAvatar.png',
                name: 'Franklin Stewart'
            },
            status: 'Open',
            due_date: 'Due today'
        },
        {
            title: 'Key client yearly offer',
            short_description: 'Hello {First name}, Thank you for being our loyal customer. In this email we would...',
            priority: 'Deal',
            step: '$400K',
            user: {
                logo: 'images/UserAvatar.png',
                name: 'Jenny Wilson'
            },
            status: 'Negotiation',
            due_date: 'Due today'
        }
    ],
    details: [
        {
            name: 'ID',
            value: '480408'
        },
        {
            name: 'Status',
            value: 'Open'
        },
        {
            name: 'Assignee',
            value: 'Robert Overit'
        },
        {
            name: 'Priority',
            value: 'High'
        },
        {
            name: 'Inbox',
            value: 'support@company.com '
        },
        {
            name: 'Associated with',
            value: [1]
        },
        {
            name: 'Due date',
            value: 'in 8 days'
        },
        {
            name: 'Followers',
            value: [{
                logo: ''
            }]
        },
        {
            name: 'Tags',
            value: ['account', 'issue']
        }
    ]
}

module.exports = {
    ticketPlaceholder
}