export interface Social {
    icon: string
}

export interface Author {
    name: string,
    email: string,
    socials: Social[],
    logo?: string,
    company: string,
}

export interface Task {
    title: string,
    short_description: string,
    priority: string,
    step: string,
    user: {
        name: string,
        logo ?: string
    },
    status: string,
    due_date: string
}

export interface TicketDetails {
    name: string,
    value: any
}

export interface Ticket {
    header: string,
    author: Author,
    published_at: string,
    tasks: Task[],
    details: TicketDetails[]
}