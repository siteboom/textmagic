export interface User{
    id: number,
    name: string
}

export interface Follower extends User {
    logo: string
}