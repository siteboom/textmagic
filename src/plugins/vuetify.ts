import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#3f51b5',
                secondary: '#b0bec5',
                accent: '#8c9eff',
                error: '#b71c1c',


                blue: '#1192FC',
                placeholder: '#A0A3BD',
                inputBg: '#F7F7FC',
                grey: '#A0A3BD',
                badgeBg: '#ED6A61',
                chipBg: '#F7F7FC',
                neutral: '#4E4B66',
                distinct: '#6E7191',
                action: '#008CFF',
                attention: '#ED6A61',

                orange: '#FF8C00'
            },
        },
    },
});
